﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAppMvc;
using WebAppMvc.Models;

namespace WebAppMvcTest
{
    public class SocialNetWorkEntitiesMock : ISocialNetWorkEntities
    {
        public virtual IQueryable<AspNetRole> AspNetRoles { get; set; }
        public virtual IQueryable<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual IQueryable<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual IQueryable<AspNetUser> AspNetUsers { get; set; }
        public virtual IQueryable<UsersHasFriend> UsersHasFriends { get; set; }
        public virtual IQueryable<Message> Messages { get; set; }
        public virtual IQueryable<AdditionalUserInfo> AdditionalUserInfoes { get; set; }

        public SocialNetWorkEntitiesMock()
        {
            AspNetRoles = new List<AspNetRole>() 
            { new AspNetRole()
                { 
                } 
            }.AsQueryable();
            AspNetUserClaims = new List<AspNetUserClaim>() 
            { new AspNetUserClaim()
                { 
                } 
            }.AsQueryable();
            AspNetUserLogins = new List<AspNetUserLogin>() 
            { new AspNetUserLogin()
                { 
                } 
            }.AsQueryable();
            AspNetUsers = new List<AspNetUser>() 
            { new AspNetUser()
                { 
                } 
            }.AsQueryable();
            UsersHasFriends = new List<UsersHasFriend>() 
            { new UsersHasFriend()
                { 
                } 
            }.AsQueryable();
            Messages = new List<Message>() 
            { new Message()
                { 
                } 
            }.AsQueryable();
            AdditionalUserInfoes = new List<AdditionalUserInfo>() 
            { new AdditionalUserInfo()
                { 
                } 
            }.AsQueryable();
        }

    }
}
