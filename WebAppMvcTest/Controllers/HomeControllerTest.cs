﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebAppMvc.Controllers;
using WebAppMvc;
using System.Web.Mvc;
using System.Collections.Generic;

namespace WebAppMvcTest.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        #region _FriendsPartial
        [TestMethod]
        public void _FriendsPartial_NotLogin_POST()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ActionResult result = controller._FriendsPartial(new List<IQueryable> { }, notLogin: true);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            RedirectToRouteResult resultAsRedirect = result as RedirectToRouteResult;
            Assert.AreEqual("Login", resultAsRedirect.RouteValues["action"]);
        }

        [TestMethod]
        public void _FriendsPartial_Login_POST()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ActionResult result = controller._FriendsPartial(new List<IQueryable> { }, notLogin: false);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            RedirectToRouteResult resultAsRedirect = result as RedirectToRouteResult;
            Assert.AreEqual("Index", resultAsRedirect.RouteValues["action"]);
        }

        [TestMethod]
        public void _FriendsPartial_NotLogin_GET()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ActionResult result = controller._FriendsPartial(notLogin: true);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            RedirectToRouteResult resultAsRedirect = result as RedirectToRouteResult;
            Assert.AreEqual("Login", resultAsRedirect.RouteValues["action"]);
        }

        [TestMethod]
        public void _FriendsPartial_Login_GET()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller._FriendsPartial(notLogin: false) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            ViewDataDictionary viewData = result.ViewData;
            Assert.IsNotNull(viewData);
            Assert.AreEqual("UsersHasFriends", viewData["Title"]);
        }
        #endregion

        #region Index
        [TestMethod]
        public void Index_NotLogin()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ActionResult result = controller.Index(notLogin: true);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            RedirectToRouteResult resultAsRedirect = result as RedirectToRouteResult;
            Assert.AreEqual("Login", resultAsRedirect.RouteValues["action"]);
        }

        [TestMethod]
        public void Index_Login()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index(notLogin: false) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            ViewDataDictionary viewData = result.ViewData;
            Assert.IsNotNull(viewData);
            Assert.AreEqual("Главная", viewData["Title"]);
        }
        #endregion

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            ViewDataDictionary viewData = result.ViewData;
            Assert.IsNotNull(viewData);
            Assert.AreEqual("На этой станице краткое описание сайта.", viewData["Message"]);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            ViewDataDictionary viewData = result.ViewData;
            Assert.IsNotNull(viewData);
            Assert.AreEqual("На этой станице контактная информация.", viewData["Message"]);
        }
    }
}