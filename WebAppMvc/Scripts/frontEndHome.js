﻿

var exists_jQuery = (((typeof (jQuery) != 'undefined') && (jQuery)) ? true : false);

// Exception если нет jQuery
function ExceptionIfNotExist_jQuery() {
    var result = false;
    if (exists_jQuery) return result = true;
    else {
        throw ({
            message: "jQuery не доступен ('jQuery' - определение отсутствует)",
            description: "idTxtBoxFindPeople.change(): 'jQuery' - определение отсутствует)",
            name: "TypeUserError",
            number: 0
        });
    }
}

// Вазидация значения в текстовом поле на максимальное кол-во символов
// idTxtBoxControl: ИД текстового поля
// maxValue: максимальное кол-во символов
// fldName: имя поля для alert
function validateMaxLengthTxt(idTxtBoxControl, maxValue, fldName)
{
    var result = false;
    try {
        ExceptionIfNotExist_jQuery();
        if ((typeof (idTxtBoxControl) == 'string' && idTxtBoxControl.length > 0) &&
              (typeof (maxValue) == 'number' && isFinite(maxValue))) {

            if (typeof (fldName) == 'string' && fldName.length > 0) {
                fldName = 'поля "' + fldName + '"';
            }
            else {
                fldName = 'поля';
            }
            var txtBoxControl = $(idTxtBoxControl);
            if (txtBoxControl) {
                txtBoxValue = txtBoxControl.val();
                if (typeof (txtBoxValue) == 'string') {
                    if ((txtBoxValue.length >= 1) &&
                          (txtBoxValue.length <= maxValue)) {
                        result = true;
                    }
                    else {
                        alert('Значение ' + fldName +
                            ' должно быть\r\nот 1 до ' + maxValue + ' символов.');
                    }
                }
            }
        }
    }
    catch (e) {
        alert('validateMaxLengthTxt:\r\n' + e.message);
    }
    return result;
}

// Для $.post и $.get проверяет и корректирует при необходимости 
// допустимый тип данных возвращаемых данных
// returnDataType: тип возвращаемых данных
// return: допустимый тип данных возвращаемых данных (по умолчанию html)
function getValidReturnDataType(returnDataType) {
    var result = 'html';
    try {
        if ((typeof (returnDataType) == 'string') && (returnDataType.length > 0)) {
            var retType = returnDataType.toLowerCase();
            switch (retType) {
                case 'xml':
                    {
                        result = retType;
                        break;
                    }
                case 'json':
                    {
                        result = retType;
                        break;
                    }
                case 'script':
                    {
                        result = retType;
                        break;
                    }
                case 'text':
                    {
                        result = retType;
                        break;
                    }
            }
        }
    }
    catch (e) {
        alert('getValidReturnDataType:\r\n' + e.message);
    }
    return result;
}

// Возвращает случайное значение от min до max
// по умолчанию 0
function getRandomIntValue(min, max) {
    var result = 0;
    try {
        result = Math.floor(Math.random() * (max - min + 1) + min);
    }
    catch (e) {
        alert('getRandomIntValue:\r\n' + e.message);
    }
    return result;
}

// Обновление по Ajax POST запросу
// idUpdateControl: ИД обновляемого элемента
// refURL: URL хендлера
// sentData: передаваемые в запрос данные
// fCallBackSuccess: CallBack функция при Success ответе сервера
// returnDataType: тип возвращаемых данных
function updateControlAjaxPOST(idUpdateControl, refURL, sentData, fCallBackSuccess, returnDataType) {
    try {
        ExceptionIfNotExist_jQuery();
        if ((typeof (idUpdateControl) == 'string') && (idUpdateControl.length > 0) &&
             (typeof (refURL) == 'string') && (refURL.length > 0) &&
              (typeof (sentData) == 'object') && (typeof (fCallBackSuccess) == 'function')) {
            var self = $(idUpdateControl);
            if (self) {
                var retDataType = 'html';
                if ((typeof (returnDataType) == 'string') && (returnDataType.length > 0)) {
                    retDataType = returnDataType
                }
                $.post(refURL, sentData, fCallBackSuccess, getValidReturnDataType(retDataType));
            }
        }
    }
    catch (e) {
        alert('updateControlAjaxPOST:\r\n' + e.message);
    }
    return;
}

// Обновление по Ajax GET запросу
// idUpdateControl: ИД обновляемого элемента
// refURL: URL хендлера
// sentData: передаваемые в запрос данные
// fCallBackSuccess: CallBack функция при Success ответе сервера
// returnDataType: тип возвращаемых данных
function updateControlAjaxGET(idUpdateControl, refURL, sentData, fCallBackSuccess, returnDataType) {
    try {
        ExceptionIfNotExist_jQuery();
        if ((typeof (idUpdateControl) == 'string') && (idUpdateControl.length > 0) &&
             (typeof (refURL) == 'string') && (refURL.length > 0) &&
              (typeof (sentData) == 'object') && (typeof (fCallBackSuccess) == 'function')) {
            var self = $(idUpdateControl);
            if (self) {
                var retDataType = 'html';
                if ((typeof (returnDataType) == 'string') && (returnDataType.length > 0)) {
                    retDataType = returnDataType
                }
                $.get(refURL + '?query=' + getRandomIntValue(1, 100000),
                    sentData, fCallBackSuccess, getValidReturnDataType(retDataType));
            }
        }
    }
    catch (e) {
        alert('updateControlAjaxGET:\r\n' + e.message);
    }
    return;
}

// Вазидация значения в текстовом поле на максимальное кол-во символов
// idBtnControl: ИД элемента по экшену click которого выполняется валидация
// idTxtBoxControl: ИД текстового поля
// maxValue: максимальное кол-во символов
// fldName: имя поля для alert
function setValidateMaxLengthTxt(idBtnControl, idTxtBoxControl, maxValue, fldName) {
    var result = false;
    try {
        ExceptionIfNotExist_jQuery();
        if ((typeof (idBtnControl) == 'string') && (idBtnControl.length > 0)) {
            var btnControl = $(idBtnControl);
            if (btnControl) {
                if (typeof (fldName) == 'string' && fldName.length > 0) {
                    fldName = 'поля "' + fldName + '"';
                }
                else {
                    fldName = 'поля';
                }
                btnControl.click(function () {
                    var result = false;
                    try {
                        var txtBoxControl = $(idTxtBoxControl);
                        if (txtBoxControl) {
                            txtBoxValue = txtBoxControl.val();
                            if (typeof (txtBoxValue) == 'string') {
                                if ((txtBoxControl.val().length >= 1) &&
                                    (txtBoxControl.val().length <= maxValue)) {
                                    result = true;
                                }
                                else {
                                    alert('Значение ' + fldName +
                                        ' должно быть\r\nот 1 до ' + maxValue + ' символов.');
                                }
                            }
                        }
                    }
                    catch (e) {
                        alert('setValidateMaxLengthTxt():\r\n' + e.message);
                    }
                    return result;
                    });
                result = true;
            }
        }
    }
    catch (e) {
        alert('setValidateMaxLengthTxt:\r\n' + e.message);
    }
    return result;
}

// Получение значения текстового поля, указанного его ИД
// idControl: ИД текстового поля
function extractValueById(idControl) {
    var result = null;
    try {
        ExceptionIfNotExist_jQuery();
        if ((typeof (idControl) == 'string') && (idControl.length > 0)) {
            var id_Control = $(idControl);
            if (id_Control) {
                var extractValue = id_Control.val();
                if (typeof (extractValue) != 'undefined') {
                    result = { id: extractValue };
                }
            }
        }
    }
    catch (e) {
        alert('extractValueById:\r\n' + e.message);
    }
    return result;
}

// Перемещение скроллинга элемента
// idControlScrolling: ИД элемента скролирования
// idMarker: ИД маркера разметки
function scrollToMarkerById(idControlScrolling, idMarker) {
    var result = false;
    try {
        ExceptionIfNotExist_jQuery();
        if ((typeof (idControlScrolling) == 'string' && idControlScrolling.length > 0) &&
            (typeof (idMarker) == 'string' && idMarker.length > 0)) {
            var id_Marker = $(idMarker);
            var id_ControlScrolling = $(idControlScrolling);
            if ((id_Marker) && (id_ControlScrolling)) {
                var offset_Marker = id_Marker.offset();
                if (offset_Marker) {
                    id_ControlScrolling.scrollTop(id_Marker.offset().top);
                    result = true;
                }
            }
        }
    }
    catch (e) {
        alert('scrollToMarkerById:\r\n' + e.message);
    }
    return result;
}
