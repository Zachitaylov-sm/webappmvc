﻿using System.ComponentModel.DataAnnotations;

namespace WebAppMvc.Models
{
    public class FindPeopleViewModel
    {
        [Required]
        [StringLength(50, ErrorMessage = "Введите от 1 до 50 символов.", MinimumLength = 1)]
        [DataType(DataType.Text)]
        [Display(Name = "Поиск")]
        public string SearchPeople { get; set; }
    }

    public class SentMessageViewModel
    {
        [Required]
        [StringLength(256, ErrorMessage = "Введите сообщение не более 256 символов.", MinimumLength = 1)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Новое сообщение")]
        public string UserMessage { get; set; }
    }

}