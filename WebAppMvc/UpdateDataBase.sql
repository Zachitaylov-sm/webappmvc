
---------------------------------------------------------------------------
--                              Update DataBase
---------------------------------------------------------------------------
-- USE [SocialNetWork]
USE [dbfc257b3a2b734d19bf35a2d000a4a034]
GO

-------- DROP FK -----------

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UsersHasFriends_UserId_AspNetUsers_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[UsersHasFriends]'))
ALTER TABLE [dbo].[UsersHasFriends] DROP CONSTRAINT [FK_UsersHasFriends_UserId_AspNetUsers_Id]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AdditionalUserInfo_UserId_AspNetUsers_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[AdditionalUserInfo]'))
ALTER TABLE [dbo].[AdditionalUserInfo] DROP CONSTRAINT [FK_AdditionalUserInfo_UserId_AspNetUsers_Id]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Messages_FriendsId_UsersHasFriends_FriendsId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Messages]'))
ALTER TABLE [dbo].[Messages] DROP CONSTRAINT [FK_Messages_FriendsId_UsersHasFriends_FriendsId]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Messages__MsgRea__37A5467C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Messages] DROP CONSTRAINT [DF__Messages__MsgRea__37A5467C]
END
GO

-------- DROP TABLE -----------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsersHasFriends]') AND type in (N'U'))
DROP TABLE [dbo].[UsersHasFriends]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AdditionalUserInfo]') AND type in (N'U'))
DROP TABLE [dbo].[AdditionalUserInfo]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Messages]') AND type in (N'U'))
DROP TABLE [dbo].[Messages]
GO

-------- CREATE TABLE -----------

CREATE TABLE [dbo].[UsersHasFriends](
	[FriendsId] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_UsersHasFriends] PRIMARY KEY CLUSTERED 
(
	[FriendsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[AdditionalUserInfo](
	[UserId] [nvarchar](128) NOT NULL,
	[UserLastName] [nvarchar](max) NOT NULL,
	[UserFirstName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_AdditionalUserInfo] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Messages](
	[MsgId] [int] IDENTITY(1,1) NOT NULL,
	[FriendsId] [nvarchar](128) NOT NULL,
	[Msg] [nvarchar](max) NOT NULL,
	[DateSent] [datetime] NOT NULL,
	[MsgRead] [bit] NOT NULL,
 CONSTRAINT [PK_Messages] PRIMARY KEY CLUSTERED 
(
	[MsgId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-------- CREATE FK -----------

ALTER TABLE [dbo].[UsersHasFriends]  WITH CHECK ADD  CONSTRAINT [FK_UsersHasFriends_UserId_AspNetUsers_Id] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[UsersHasFriends] CHECK CONSTRAINT [FK_UsersHasFriends_UserId_AspNetUsers_Id]
GO

ALTER TABLE [dbo].[AdditionalUserInfo]  WITH CHECK ADD  CONSTRAINT [FK_AdditionalUserInfo_UserId_AspNetUsers_Id] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AdditionalUserInfo] CHECK CONSTRAINT [FK_AdditionalUserInfo_UserId_AspNetUsers_Id]
GO

ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_Messages_FriendsId_UsersHasFriends_FriendsId] FOREIGN KEY([FriendsId])
REFERENCES [dbo].[UsersHasFriends] ([FriendsId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_Messages_FriendsId_UsersHasFriends_FriendsId]
GO

-- ALTER TABLE [dbo].[Messages] ADD  DEFAULT ((0)) FOR [MsgRead]
-- GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUsers]') AND type in (N'U'))
DELETE [dbo].[AspNetUsers]
GO
