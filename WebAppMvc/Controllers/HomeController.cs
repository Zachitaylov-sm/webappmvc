﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppMvc.Models;

namespace WebAppMvc.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
            : this(new SocialNetWorkEntities())
        {
        }

        public HomeController(SocialNetWorkEntities context)
        {
            DBContext = context;
            if (DBContext != null)
            {
                ViewContext = new List<IQueryable>() 
                { DBContext.AspNetUsers, 
                  DBContext.UsersHasFriends, 
                  DBContext.Messages };
            }
        }

        /// <summary>
        /// Контекст БД
        /// </summary>
        public SocialNetWorkEntities DBContext { get; private set; }
        /// <summary>
        /// Контекст БД для Views
        /// </summary>
        public List<IQueryable> ViewContext { get; private set; }

        protected override void Dispose(bool disposing)
        {
            if (disposing && DBContext != null)
            {
                DBContext.Dispose();
                DBContext = null;
                ViewContext.Clear();
                ViewContext = null;
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Установка флага - сообщение просмотрено (прочтно) пользователем
        /// </summary>
        /// <param name="usrCurrentId"></param> // ИД текущего пользователя
        /// <param name="usrFriendId"></param>  // ИД друга, сообщения которого просмотрены текущим пользователем
        /// <returns></returns>
        private bool readMessages(string usrCurrentId, string usrFriendId)
        {
            bool result = false;
            try
            {
                if (!string.IsNullOrWhiteSpace(usrCurrentId) && !string.IsNullOrWhiteSpace(usrFriendId))
                {
                    if (isUsersFrieds(usrCurrentId, usrFriendId))
                    {
                        if ((DBContext != null) && (DBContext.Messages != null))
                        {
                            int countMakeRead = 0;
                            foreach (Message message in DBContext.Messages.Where(msg =>
                                ((msg.FriendsId == usrFriendId + "_" + usrCurrentId)) && !msg.MsgRead))
                            {
                                message.MsgRead = true;
                                ++countMakeRead;
                            }
                            if (countMakeRead > 0)
                            {
                                result = (DBContext.SaveChanges() == countMakeRead);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // вывод в лог.
                // ...
                //
            }
            return result;
        }

        /// <summary>
        /// Обработка запроса на удаление из друзей
        /// </summary>
        /// <param name="usrCurrentId"></param>            // ИД текущего пользователя
        /// <param name="usrQueryRemoveFriendId"></param>  // ИД друга, запрос на удаление которого поступил 
        /// <returns></returns>
        private bool removeFriend(string usrCurrentId, string usrQueryRemoveFriendId)
        {
            bool result = false;
            try
            {
                if (!string.IsNullOrWhiteSpace(usrCurrentId) && !string.IsNullOrWhiteSpace(usrQueryRemoveFriendId))
                {
                    if (isUsersFrieds(usrCurrentId, usrQueryRemoveFriendId))
                    {
                        if ((DBContext != null) && (DBContext.UsersHasFriends != null))
                        {
                            UsersHasFriend removeUser = DBContext.UsersHasFriends.Where(usr => usr.FriendsId == usrCurrentId + "_" + usrQueryRemoveFriendId).FirstOrDefault();
                            UsersHasFriend removeUserFriend = DBContext.UsersHasFriends.Where(usr => usr.FriendsId == usrQueryRemoveFriendId + "_" + usrCurrentId).FirstOrDefault();

                            if ((removeUser != null) && (removeUserFriend != null))
                            {
                                DBContext.UsersHasFriends.Remove(removeUser);
                                DBContext.UsersHasFriends.Remove(removeUserFriend);
                            }

                            result = (DBContext.SaveChanges() == 2);

                            if (result)
                            {
                                SessionUser.RemoveSessionKey(Session,
                                    SessionUser.GetSessionKey(SessionUser.SessionKey.SessionFriendKey));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // вывод в лог.
                // ...
                //
            }
            return result;
        }

        /// <summary>
        /// Обработка запроса на добавление в друзья 
        /// </summary>
        /// <param name="usrCurrentId"></param>      // ИД текущего пользователя
        /// <param name="usrQueryFriendId"></param>  // ИД пользователя в запросе на добавление в друзья
        /// <returns></returns>
        private bool addFriend(string usrCurrentId, string usrQueryFriendId)
        {
            bool result = false;
            try
            {
                if (!string.IsNullOrWhiteSpace(usrCurrentId) && !string.IsNullOrWhiteSpace(usrQueryFriendId))
                {
                    if (!isUsersFrieds(usrCurrentId, usrQueryFriendId))
                    {
                        if ((DBContext != null) && (DBContext.UsersHasFriends != null))
                        {
                            DBContext.UsersHasFriends.Add(new UsersHasFriend()
                            {
                                FriendsId = usrCurrentId + "_" + usrQueryFriendId,
                                UserId = usrCurrentId
                            });

                            DBContext.UsersHasFriends.Add(new UsersHasFriend()
                            {
                                FriendsId = usrQueryFriendId + "_" + usrCurrentId,
                                UserId = usrQueryFriendId
                            });

                            result = (DBContext.SaveChanges() == 2);

                            if (result)
                            {
                                SessionUser.AddSessionKey(Session, usrQueryFriendId,
                                    SessionUser.GetSessionKey(SessionUser.SessionKey.SessionFriendKey));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // вывод в лог.
                // ...
                //
            }
            return result;
        }

        /// <summary>
        /// Отправка сообщения другу
        /// </summary>
        /// <param name="usrFromId"></param> // ИД текущего пользователя
        /// <param name="usrToId"></param>   // ИД пользователя, которому отправляем сообщение
        /// <param name="message"></param>   // текст сообщения
        /// <returns></returns>
        private bool sentMessage(string usrFromId, string usrToId, string message)
        {
            bool result = false;
            try
            {
                if (!string.IsNullOrWhiteSpace(usrFromId) && !string.IsNullOrWhiteSpace(usrToId) &&
                    !string.IsNullOrWhiteSpace(message))
                {
                    if (isUsersFrieds(usrFromId, usrToId))
                    {
                        if ((DBContext != null) && (DBContext.Messages != null))
                        {
                            DBContext.Messages.Add(new Message()
                            {
                                FriendsId = usrFromId + "_" + usrToId,
                                Msg = message,
                                DateSent = DateTime.Now,
                                MsgRead = false
                            });
                            result = (DBContext.SaveChanges() == 1);
                        }
                    }
                    else
                    {
                        SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionFriendKey));
                    }
                }
            }
            catch (Exception ex)
            {
                // вывод в лог.
                // ...
                //
            }
            return result;
        }

        /// <summary>
        /// Является ли пользователь другом текущему пользователю
        /// </summary>
        /// <param name="usrCurrentId"></param> // ИД текущего пользователя
        /// <param name="usrFriendId"></param>  // ИД возможного друга
        /// <returns></returns>
        private bool isUsersFrieds(string usrCurrentId, string usrFriendId)
        {
            bool result = false;
            try
            {
                if ((DBContext != null) && !string.IsNullOrWhiteSpace(usrCurrentId) &&
                       !string.IsNullOrWhiteSpace(usrFriendId) && (usrCurrentId != usrFriendId))
                {
                    string refKeyUser = usrCurrentId + "_" + usrFriendId;
                    string refKeyFriend = usrFriendId + "_" + usrCurrentId;
                    result = ((DBContext.UsersHasFriends.FirstOrDefault(usrRef => usrRef.FriendsId == refKeyUser) != null) &&
                        (DBContext.UsersHasFriends.FirstOrDefault(usrRef => usrRef.FriendsId == refKeyFriend) != null));
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public ActionResult Index(int id = 0, bool? notLogin = null)
        {
            if (((notLogin == null) ? true : (bool)notLogin) &&
                 !SessionUser.isExistSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)))
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                ViewBag.Title = "Главная";

                return View(ViewContext);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "На этой станице краткое описание сайта.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "На этой станице контактная информация.";

            return View();
        }

        /// <summary>
        /// GET: /Home/_FriendsPartial 
        /// При просмотре сообщений друга 
        /// Ajax вызов для обновления _FriendsPartial 
        /// (сообщения о новых письмах)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult _FriendsPartial(string id = null, bool? notLogin = null)
        {
            // для анонимного пользователя перенаправление на регистрацию
            if (((notLogin == null) ? true : (bool)notLogin) && 
                !SessionUser.isExistSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)))
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                // 
                if ((DBContext != null) && (DBContext.UsersHasFriends != null))
                {
                    ViewBag.Title = "UsersHasFriends";

                    return View(DBContext.UsersHasFriends);
                }
                else
                {
                    return View();
                }
                //return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// POST: /Home/_FriendsPartial
        /// Удаление из друзей - action в _FriendsPartial 
        /// </summary>
        /// <param name="model"></param> 
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult _FriendsPartial(IList<IQueryable> model, bool? notLogin = null)
        {
            // для анонимного пользователя перенаправление на регистрацию
            if (((notLogin == null) ? true : (bool)notLogin) &&
                !SessionUser.isExistSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)))
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if ((RouteData != null) && (RouteData.Values != null))
                {
                    object idForm = RouteData.Values["id"];
                    string id_frm = ((idForm != null) && (idForm is string)) ? (idForm as string) : string.Empty;
                    string usrQueryRemoveId = (!string.IsNullOrWhiteSpace(id_frm)) ? id_frm.Substring(3, 36) : string.Empty;
                    if (!string.IsNullOrWhiteSpace(usrQueryRemoveId))
                    {
                        if (!removeFriend(SessionUser.GetValueSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)), usrQueryRemoveId))
                        {
                            //
                            //
                        }
                    }
                }
                //if (model != null)
                //{
                //    return View(model);
                //}
                //else
                //{
                //    return View();
                //}
                // Обновляются:
                // _FriendsPartial (сообщения о новых письмах)
                // _PeoplePartial (сообщение, что добавлен в друзья)
                // _MessagesPartial и _SentMessagePartial 
                // (могли быть открыты для просмотра сообщения удаляемого из друзей)
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// GET: /Home/_PeoplePartial 
        /// Добавление в друзья - link в _PeoplePartial
        /// </summary>
        /// <param name="id"></param> // ИД пользователя добавляемого в друзья
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult _PeoplePartial(string id = null)
        {
            // для анонимного пользователя перенаправление на регистрацию
            if (!SessionUser.isExistSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)))
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(id))
                {
                    if (!addFriend(SessionUser.GetValueSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)), id))
                    {
                        //
                        //
                    }
                }
                // Обновляются:
                // _FriendsPartial (сообщения о новых письмах)
                // _PeoplePartial (сообщение, что добавлен в друзья)
                // _MessagesPartial и _SentMessagePartial 
                // (для нового друга - сообщений еще нет)
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// POST: /Home/_PeoplePartial 
        /// </summary>
        /// <param name="model"></param> 
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult _PeoplePartial(IQueryable<WebAppMvc.AspNetUser> model)
        {
            // для анонимного пользователя перенаправление на регистрацию
            if (!SessionUser.isExistSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)))
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //if (model != null)
                //{
                //    return View(model);
                //}
                //else
                //{
                //    return View();
                //}
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// GET: /Home/_SentMessagePartial 
        /// Отправка сообщения
        /// </summary>
        /// <param name="id"></param> // текст сообщения в get запросе
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult _SentMessagePartial(string id = null)
        {
            // для анонимного пользователя перенаправление на регистрацию
            if (!SessionUser.isExistSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)))
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(id) &&
                      (id.Length >= 1) && (id.Length <= 256))
                {
                    if (!sentMessage(SessionUser.GetValueSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)),
                          SessionUser.GetValueSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionFriendKey)),
                          id))
                    {
                        //
                        //
                    }
                }
                //return View();
                return RedirectToAction("_MessagesPartial",
                    new { id = SessionUser.GetValueSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionFriendKey)) });
            }
        }

        /// <summary>
        /// POST: /Home/_SentMessagePartial 
        /// Отправка сообщения (при обычном (не в Ajax) action)
        /// </summary>
        /// <param name="model"></param> 
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult _SentMessagePartial(SentMessageViewModel model)
        {
            // для анонимного пользователя перенаправление на регистрацию
            if (!SessionUser.isExistSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)))
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    if (!string.IsNullOrWhiteSpace(model.UserMessage) &&
                         (model.UserMessage.Length >= 1) && (model.UserMessage.Length <= 256))
                    {
                        if (!sentMessage(SessionUser.GetValueSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)),
                              SessionUser.GetValueSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionFriendKey)),
                              model.UserMessage))
                        {
                            //
                            //
                        }
                    }
                }
                //if (model != null)
                //{
                //    return View(model);
                //}
                //else
                //{
                //    return View();
                //}
                return RedirectToAction("_MessagesPartial",
                    new { id = SessionUser.GetValueSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionFriendKey)) });
            }
        }

        /// <summary>
        /// GET: /Home/_MessagesPartial
        /// Просмотр сообщений друга: 
        /// - link в _FriendsPartial
        /// - button в _MessagesPartial
        /// </summary>
        /// <param name="id"></param> // ИД друга
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult _MessagesPartial(string id = null)
        {
            // для анонимного пользователя перенаправление на регистрацию
            if (!SessionUser.isExistSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)))
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (isUsersFrieds(SessionUser.GetValueSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)), id))
                {
                    SessionUser.AddSessionKey(Session, id, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionFriendKey));
                    if (!readMessages(SessionUser.GetValueSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)), id))
                    {
                        //
                        //
                    }
                }
                else
                {
                    SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionFriendKey));
                }
                //
                // при Ajax action
                // возвращается только View(DBContext.Messages)
                //                
                if ((DBContext != null) && (DBContext.Messages != null))
                {
                    return View(DBContext.Messages);
                }
                else
                {
                    return View();
                }
                //
                // при обычном action
                //return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// POST: /Home/_MessagesPartial 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult _MessagesPartial(IList<IQueryable> model)
        {
            // для анонимного пользователя перенаправление на регистрацию
            if (!SessionUser.isExistSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)))
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (!readMessages(SessionUser.GetValueSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)),
                    SessionUser.GetValueSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionFriendKey))))
                {
                    //
                    //
                }
                //if ((DBContext != null) && (DBContext.Messages != null))
                //{
                //    return View(DBContext.Messages);
                //}
                //else
                //{
                //    if (model != null)
                //    {
                //        return View(model);
                //    }
                //    else
                //    {
                //        return View();
                //    }
                //}
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// GET: /Home/_SearchPartial 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult _SearchPartial(string id = null)
        {
            // для анонимного пользователя перенаправление на регистрацию
            if (!SessionUser.isExistSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)))
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(id) && (id.Length <= 50))
                {
                    SessionUser.AddSessionKey(Session, id, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionSearchKey));
                }
                else
                {
                    SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionSearchKey));
                }
                //return View();
                //return RedirectToAction("_PeoplePartial");
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// POST: /Home/_SearchPartial
        /// Поиск людей - button в _SearchPartial
        /// button в _SearchPartial
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult _SearchPartial(FindPeopleViewModel model)
        {
            // для анонимного пользователя перенаправление на регистрацию
            if (!SessionUser.isExistSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)))
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (ModelState.IsValid && !string.IsNullOrWhiteSpace(model.SearchPeople) && (model.SearchPeople.Length <= 50))
                {
                    SessionUser.AddSessionKey(Session, model.SearchPeople, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionSearchKey));
                }
                else
                {
                    SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionSearchKey));
                }
                //if (model != null)
                //{
                //    return View(model);
                //}
                //else 
                //{
                //    return View();
                //}
                return RedirectToAction("Index");
            }
        }

    }
}