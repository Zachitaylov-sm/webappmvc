﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using WebAppMvc.Models;

namespace WebAppMvc.Controllers
{
    /// <summary>
    /// Класс для работы с сессионными ключами
    /// </summary>
    public static class SessionUser
    {
        public enum SessionKey
        {
            SessionUserKey,          // ключ id текущего пользователя
            SessionFriendKey,        // ключ id текущего (выбранного) друга
            SessionSearchKey,        // ключ и информацией о последнем поиске людей
            SessionUserFullNameKey   // ключ с квалифицированным именем текущего пользователя
        }

        /// <summary>
        /// Справочник сессионных ключей
        /// </summary>
        private static Dictionary<SessionKey, string> SessionKeys =
            new Dictionary<SessionKey, string>() 
            { 
                {SessionKey.SessionUserKey, Guid.NewGuid().ToString()},    
                {SessionKey.SessionFriendKey, Guid.NewGuid().ToString()},  
                {SessionKey.SessionSearchKey, Guid.NewGuid().ToString()},
                {SessionKey.SessionUserFullNameKey, Guid.NewGuid().ToString()}
            };

        /// <summary>
        /// Получение фактического имени сессионного ключа
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetSessionKey(SessionKey key)
        {
            string result = string.Empty;
            try
            {
                if ((SessionKeys != null) && (SessionKeys.Count > 0))
                {
                    if (SessionKeys.ContainsKey(key))
                    {
                        SessionKeys.TryGetValue(key, out result);
                    }
                }
            }
            catch
            {
                result = string.Empty;
            }
            return result;
        }

        /// <summary>
        /// Удаление ключа из сессии
        /// </summary>
        /// <param name="session"></param>    // Сессия
        /// <param name="SessionKey"></param> // Имя ключа
        public static void RemoveSessionKey(HttpSessionStateBase session, string SessionKey)
        {
            if ((session != null) && !string.IsNullOrWhiteSpace(SessionKey))
            {
                if (session[SessionKey] != null)
                {
                    session.Remove(SessionKey);
                }
            }
        }

        /// <summary>
        /// Удаление всех (enum SessionKey) текущих ключей сессии
        /// </summary>
        /// <param name="session"></param>
        public static void RemoveAllSessionKeys(HttpSessionStateBase session)
        {
            if (session != null)
            {
                foreach (string key in SessionKeys.Values)
                {
                    RemoveSessionKey(session, key);
                }
            }
        }

        /// <summary>
        /// Добавление ключа в сессию
        /// </summary>
        /// <param name="session"></param>     // Сессия
        /// <param name="valueForKey"></param> // Значение ключа (ApplicationUser или String)
        /// <param name="SessionKey"></param>  // Имя ключа
        public static void AddSessionKey(HttpSessionStateBase session, object valueForKey, string SessionKey)
        {
            if ((session != null) && (valueForKey != null) && !string.IsNullOrWhiteSpace(SessionKey))
            {
                if (session[SessionKey] != null)
                {
                    RemoveSessionKey(session, SessionKey);
                }
                if (session[SessionKey] == null)
                {
                    if (valueForKey is ApplicationUser)
                    {                        
                        session.Add(SessionKey, (valueForKey as ApplicationUser).Id.ToString());
                    }
                    else if (valueForKey is string)
                    {
                        session.Add(SessionKey, valueForKey);
                    }
                }
            }
        }

        /// <summary>
        /// Проверка существования ключа в текущей сессии
        /// </summary>
        /// <param name="session"></param>    // Сессия
        /// <param name="SessionKey"></param> // Имя ключа
        /// <returns></returns>
        public static bool isExistSessionKey(HttpSessionStateBase session, string SessionKey)
        {
            bool result = false;
            try
            {
                if ((session != null) && !string.IsNullOrWhiteSpace(SessionKey))
                {
                    result = (session[SessionKey] != null);
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Получение значения ключа сессии по его имени
        /// </summary>
        /// <param name="session"></param>    // Сессия
        /// <param name="SessionKey"></param> // Имя ключа
        /// <returns></returns>
        public static string GetValueSessionKey(HttpSessionStateBase session, string SessionKey)
        {
            string result = string.Empty;
            try
            {
                if ((session != null) && !string.IsNullOrWhiteSpace(SessionKey) && (session[SessionKey] != null) &&
                        !string.IsNullOrWhiteSpace(session[SessionKey].ToString()))
                {

                    result = session[SessionKey].ToString();
                }
            }
            catch
            {
                result = string.Empty;
            }
            return result;
        }

    }
    
    [Authorize]
    public class AccountController : Controller
    {
        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())),
            new SocialNetWorkEntities())
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager,
            SocialNetWorkEntities context)
        {
            DBContext = context;
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }
        /// <summary>
        /// Контекст для работы с дополнительной информацией о пользователе
        /// </summary>
        public SocialNetWorkEntities DBContext { get; private set; }

        /// <summary>
        /// Сохранения дополнительной информации о пользователе
        /// </summary>
        /// <param name="UserId"></param>        // ИД текущего пользователя
        /// <param name="UserLastName"></param>  // Фамилия и Имя пользователя
        /// <param name="UserFirstName"></param>
        /// <returns></returns>
        private bool saveAdditionalUserInfo(string UserId, string UserLastName, string UserFirstName)
        {
            bool result = false;
            try
            {
                if (!string.IsNullOrWhiteSpace(UserId) && !string.IsNullOrWhiteSpace(UserLastName) &&
                    !string.IsNullOrWhiteSpace(UserFirstName))
                {
                    if ((DBContext != null) && (DBContext.AdditionalUserInfoes != null))
                    {
                        DBContext.AdditionalUserInfoes.Add(new AdditionalUserInfo()
                        {
                            UserId = UserId,
                            UserLastName = UserLastName,
                            UserFirstName = UserFirstName
                        });

                        result = (DBContext.SaveChanges() == 1);
                    }
                }
            }
            catch (Exception ex)
            {
                // вывод в лог.
                // ...
                //
            }
            return result;
        }

        /// <summary>
        /// Получение квалифицированного имени пользователя
        /// </summary>
        /// <param name="UserId"></param> // ИД текущего пользователя
        /// <returns></returns>
        private string getFullUserName(string UserId)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrWhiteSpace(UserId))
                {
                    if ((DBContext != null) && (DBContext.AdditionalUserInfoes != null))
                    {
                        AdditionalUserInfo addUserInfo = DBContext.AdditionalUserInfoes.Where(usrInfo => usrInfo.UserId == UserId).FirstOrDefault();
                        if (addUserInfo != null)
                        {
                            result = addUserInfo.UserLastName + " " + addUserInfo.UserFirstName;
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                // вывод в лог.
                // ...
                //
            }
            return result;
        }

        [AllowAnonymous]
        public ActionResult Index(string returnUrl = null)
        {
            ViewBag.ReturnUrl = returnUrl;
            if (!SessionUser.isExistSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey)))
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
        }
        
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
                    await SignInAsync(user, model.RememberMe);
                    SessionUser.AddSessionKey(Session, user, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey));
                    SessionUser.AddSessionKey(Session, getFullUserName(user.Id), 
                        SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserFullNameKey));
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey));
                    SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserFullNameKey));
                    ModelState.AddModelError("", "Неверные логин или пароль.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInAsync(user, isPersistent: false);
                    saveAdditionalUserInfo(user.Id, model.UserLastName, model.UserFirstName);
                    SessionUser.AddSessionKey(Session, user, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey));
                    SessionUser.AddSessionKey(Session, getFullUserName(user.Id),
                        SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserFullNameKey));
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey));
                    SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserFullNameKey));
                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey));
                SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserFullNameKey));
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Ваш пароль был изменен."
                : message == ManageMessageId.SetPasswordSuccess ? "Ваш пароль был сохранен."
                : message == ManageMessageId.RemoveLoginSuccess ? "Внешняя учетная запись была удалена."
                : message == ManageMessageId.Error ? "Произошла ошибка."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey));
                SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserFullNameKey));
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                SessionUser.AddSessionKey(Session, user, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey));
                SessionUser.AddSessionKey(Session, getFullUserName(user.Id),
                    SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserFullNameKey));
                return RedirectToLocal(returnUrl);
            }
            else
            {
                SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey));
                SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserFullNameKey));
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey));
                SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserFullNameKey));
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey));
                    SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserFullNameKey));
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);
                        SessionUser.AddSessionKey(Session, user, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey));
                        SessionUser.AddSessionKey(Session, getFullUserName(user.Id),
                            SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserFullNameKey));
                        return RedirectToLocal(returnUrl);
                    }
                }
                SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserKey));
                SessionUser.RemoveSessionKey(Session, SessionUser.GetSessionKey(SessionUser.SessionKey.SessionUserFullNameKey));
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            SessionUser.RemoveAllSessionKeys(Session);
            return RedirectToAction("About", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (UserManager != null)
                {
                    UserManager.Dispose();
                    UserManager = null;
                }
                if (DBContext != null)
                {
                    DBContext.Dispose();
                    DBContext = null;
                }
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}
